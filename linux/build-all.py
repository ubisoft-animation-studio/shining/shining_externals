#!/bin/python3

import os
import shutil
from contextlib import contextmanager
import sys
import locale
import urllib2
import argparse

LLVM_VERSION="5.0.2"
LLVM_SUFFIX = LLVM_VERSION.replace(".", "_")

OIIO_VERSION = "1.8.14"
OIIO_SHORT_VERSION = OIIO_VERSION[0:OIIO_VERSION.rfind(".")]
OIIO_SUFFIX = OIIO_VERSION.replace(".", "_")

OSL_VERSION = "1.9.10"
OSL_SHORT_VERSION = OSL_VERSION[0:OSL_VERSION.rfind(".")]
OSL_SUFFIX = OSL_VERSION.replace(".", "_")

EMBREE_VERSION = "2.12.0"
EMBREE_SHORT_VERSION = EMBREE_VERSION[0:EMBREE_VERSION.rfind(".")]
EMBREE_SUFFIX = EMBREE_VERSION.replace(".", "_")

BUILD_TYPES = [ "Release" ]

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

ROOT_DIR = os.getcwd()
CMAKE_INSTALL_PREFIX = [ os.path.join(ROOT_DIR, "dist-" + build_type) for build_type in BUILD_TYPES ]
SHINING_LIB_DIR = os.path.join(ROOT_DIR, "shining-externals")

SHINING_3RD_PARTY_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../precompiled/ShiningGMake")

# Global variables set in main from command arguments or default values:
BUILD_DIR_SUFFIX = []
CMAKE_GENERATOR = ""
THIRD_PARTY_DIR = ""

def mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)

def copy(src, dst):
    if os.path.isdir(src):
        if os.path.exists(dst):
            copyfiles(src, dst)
        else:
            print("cp " + src + " -> " + dst)
            shutil.copytree(src, dst)
    else:
        print("cp " + src + " -> " + dst)
        shutil.copy(src, dst)

def copyfiles(dir, dstDir):
    for entry in os.listdir(dir):
        copy(os.path.join(dir, entry), os.path.join(dstDir, entry))

def git(command):
    os.system("git " + command)

def cmake(command):
    os.system('cmake -G "' + CMAKE_GENERATOR + '" ' + command)

def build(target, config):
    os.system("make -j 16 " + target)

def safeUrlOpen(url):
    try:
        return urllib2.urlopen(url)
    except urllib2.HTTPError as e:
        print ("HTTP Error: ", e.code, url)
        exit()
    except urllib2.URLError as e:
        print ("URLError: ", e.reason, url)
        exit()

def call7zip(command):
    os.system("7za " + command)

@contextmanager
def pushd(newDir):
    previousDir = os.getcwd()
    os.chdir(newDir)
    yield
    os.chdir(previousDir)

def valid_list(str):
     value = str.split(",")
     return value

def get_precompiled_libraries():
    for install_prefix in CMAKE_INSTALL_PREFIX:
        copyfiles(os.path.join(THIRD_PARTY_DIR, "lib", "x64"), install_prefix + "/lib")
        copyfiles(os.path.join(THIRD_PARTY_DIR, "include"), install_prefix + "/include")
    
    copy(THIRD_PARTY_DIR + "/libpng-1.6.8.tgz", SHINING_LIB_DIR + "/libpng-1.6.8.tgz")

    # boost is a special case: we want to link to precompiled binaries, because boost_regex is linked to precompiled libiccuc.so.48
    # but we need all headers to compiled OSL and OIIO
    copy(THIRD_PARTY_DIR + "/boost_1_57_0.tgz", SHINING_LIB_DIR + "/boost_1_57_0.tgz")
    copy(THIRD_PARTY_DIR + "/boost_1_57_0.tgz", "boost_1_57_0.tgz")
    os.system("tar xvf boost_1_57_0.tgz")
    for install_prefix in CMAKE_INSTALL_PREFIX:
        copyfiles("boost_1_57_0/stage/lib", install_prefix + "/lib")
    shutil.rmtree("boost_1_57_0")

    copy(SHINING_3RD_PARTY_DIR + "/boost_1_57_0.7z", "boost_1_57_0.7z")
    call7zip("x boost_1_57_0.7z")
    for install_prefix in CMAKE_INSTALL_PREFIX:
        copy("boost_1_57_0/boost", install_prefix + "/include/boost")
    shutil.rmtree("boost_1_57_0")

    with pushd(SHINING_LIB_DIR):
        mkdir("additional_centos7")
        copyfiles(os.path.join(THIRD_PARTY_DIR, "lib", "x64"), "additional_centos7")
        os.system("tar zcfv additional_centos7.tgz additional_centos7")

def build_openexr():
    PRECOMPILED_DIR = "openexr-2.2"

    LIB_NAME="OpenEXR_2_2"
    GIT_TAG="v2.2.0"
    CLONE_DIR="repo-" + LIB_NAME
    if not os.path.exists(CLONE_DIR):
        git("clone https://github.com/openexr/openexr " + CLONE_DIR)
        with pushd(CLONE_DIR):
            git('config user.email "laurent.noel@ubisoft.com"')
            git('config user.name "Laurent NOEL"')
            git("checkout " + GIT_TAG)

    INSTALL_PREFIX = [ ROOT_DIR+"/dist-" + LIB_NAME + x for x in BUILD_DIR_SUFFIX ]

    for LIB_NAME in [ "Ilmbase_2_2", "OpenEXR_2_2" ]:
        BUILD_DIR = [ ROOT_DIR + "/build-" + LIB_NAME + x for x in BUILD_DIR_SUFFIX ]
        
        for dir in INSTALL_PREFIX:
            mkdir(dir)
        for dir in BUILD_DIR:
            mkdir(dir)

        for i, build_type in enumerate(BUILD_TYPES):
            with pushd(BUILD_DIR[i]):
                if LIB_NAME == "Ilmbase_2_2":
                    cmake(" -DCMAKE_BUILD_TYPE=" + build_type +
                          " -DCMAKE_INSTALL_PREFIX=" + INSTALL_PREFIX[i] +
                          " -DCMAKE_PREFIX_PATH=" + CMAKE_INSTALL_PREFIX[i] +
                          " ../" + CLONE_DIR + "/IlmBase")
                else:
                    cmake(" -DCMAKE_BUILD_TYPE=" + build_type + 
                          " -DCMAKE_INSTALL_PREFIX=" + INSTALL_PREFIX[i] +
                          " -DCMAKE_PREFIX_PATH=" + CMAKE_INSTALL_PREFIX[i] +
                          " -DILMBASE_PACKAGE_PREFIX=" + CMAKE_INSTALL_PREFIX[i] + 
                          " -DZLIB_ROOT=" + CMAKE_INSTALL_PREFIX[i] + 
                          " ../" + CLONE_DIR + "/OpenEXR")
                    mkdir("IlmImf/"+build_type)
                build("install", build_type)
                copyfiles(INSTALL_PREFIX[i], CMAKE_INSTALL_PREFIX[i])

def build_llvm():
    LLVM_DIR="llvm-" + LLVM_VERSION + ".src"
    LLVM_AR_EXT = ".tar.xz"
    LLVM_AR_FILE = LLVM_DIR + LLVM_AR_EXT
    if not os.path.exists(LLVM_DIR):
        if not os.path.exists(LLVM_AR_FILE):
            print ("Downloading " + LLVM_AR_FILE)
            f = safeUrlOpen("http://llvm.org/releases/" + LLVM_VERSION + "/" + LLVM_AR_FILE)
            with open(LLVM_AR_FILE, "wb") as localFile:
                localFile.write(f.read())
            f.close()
        call7zip("x " + LLVM_AR_FILE)
        call7zip("x " + LLVM_DIR + ".tar")
        os.remove(LLVM_DIR + ".tar")

    LIB_NAME="llvm_" + LLVM_SUFFIX
    INSTALL_PREFIX = [ ROOT_DIR+"/dist-"+LIB_NAME+x for x in BUILD_DIR_SUFFIX ]
    BUILD_DIR = [ ROOT_DIR + "/build-" + LIB_NAME + x for x in BUILD_DIR_SUFFIX ]

    for dir in INSTALL_PREFIX:
        mkdir(dir)
    for dir in BUILD_DIR:
        mkdir(dir)

    for i, build_type in enumerate(BUILD_TYPES):
        with pushd(BUILD_DIR[i]):
            cmake(" -DCMAKE_BUILD_TYPE=" + build_type +
                  " -DCMAKE_INSTALL_PREFIX=" + INSTALL_PREFIX[i] +
                  " -DCMAKE_PREFIX_PATH=" + CMAKE_INSTALL_PREFIX[i] +
                  " ../" + LLVM_DIR)
            build("install", build_type)
            copyfiles(INSTALL_PREFIX[i], CMAKE_INSTALL_PREFIX[i])

def build_boost():
    BOOST_VERSION_STRING = "1.57.0"
    LIB_NAME="boost_1_57_0"
    PRECOMPILED_DIR = "boost-" + BOOST_VERSION_STRING
    INSTALL_PREFIX = [ ROOT_DIR+"/dist-"+LIB_NAME+x for x in BUILD_DIR_SUFFIX ]

    if not os.path.exists(LIB_NAME):
        if not os.path.exists(LIB_NAME + ".7z"):
            assert(os.path.exists(SHINING_3RD_PARTY_DIR + "/" + LIB_NAME + ".7z"))
            copy(SHINING_3RD_PARTY_DIR + "/" + LIB_NAME + ".7z", LIB_NAME + ".7z")
        
        call7zip("x " + LIB_NAME + ".7z")
    
    with pushd(LIB_NAME):
        os.system("./bootstrap.sh")
        for i, build_type in enumerate(BUILD_TYPES):
            os.system("./b2 --prefix=" + INSTALL_PREFIX[i] + " -j8 install")
            copyfiles(INSTALL_PREFIX[i], CMAKE_INSTALL_PREFIX[i])

    if os.path.exists(SHINING_LIB_DIR + "/" + LIB_NAME + ".tgz"):
        return

    print("Build boost archive " + SHINING_LIB_DIR + "/" + LIB_NAME + ".tgz for shining...")

    mkdir(SHINING_LIB_DIR + "/" + LIB_NAME + "/boost")
    mkdir(SHINING_LIB_DIR + "/" + LIB_NAME + "/stage/lib")

    files_to_copy = ['predef.h', 'scoped_array.hpp', 'is_placeholder.hpp', 'pointer_to_other.hpp', 'serialization', 'operators.hpp', 'type.hpp', 'memory_order.hpp', 'aligned_storage.hpp', 'integer_traits.hpp', 'cstdint.hpp', 'preprocessor', 'optional', 'progress.hpp', 'chrono', 'lexical_cast', 'limits.hpp', 'array.hpp', 'tuple', 'checked_delete.hpp', 'move', 'visit_each.hpp', 'iterator.hpp', 'algorithm', 'core', 'static_assert.hpp', 'exception_ptr.hpp', 'align', 'range', 'concept_check.hpp', 'typeof', 'thread.hpp', 'bind.hpp', 'intrusive', 'type_traits', 'function.hpp', 'call_traits.hpp', 'non_type.hpp', 'shared_array.hpp', 'math_fwd.hpp', 'io', 'token_functions.hpp', 'config.hpp', 'integer.hpp', 'math', 'mpl', 'system', 'cstdlib.hpp', 'config', 'atomic.hpp', 'version.hpp', 'multi_index_container.hpp', 'swap.hpp', 'io_fwd.hpp', 'unordered_map.hpp', 'function', 'archive', 'scoped_ptr.hpp', 'smart_ptr', 'optional.hpp', 'mem_fn.hpp', 'unordered', 'multi_index', 'detail', 'multi_index_container_fwd.hpp', 'exception', 'shared_ptr.hpp', 'thread', 'current_function.hpp', 'unordered_set.hpp', 'cerrno.hpp', 'numeric', 'assert.hpp', 'ref.hpp', 'integer_fwd.hpp', 'predef', 'token_iterator.hpp', 'timer.hpp', 'test', 'iterator', 'none.hpp', 'throw_exception.hpp', 'intrusive_ptr.hpp', 'noncopyable.hpp', 'next_prior.hpp', 'integer', 'rational.hpp', 'functional', 'ratio', 'bimap', 'utility.hpp', 'tokenizer.hpp', 'date_time', 'atomic', 'get_pointer.hpp', 'concept', 'lexical_cast.hpp', 'type_traits.hpp', 'bind', 'enable_shared_from_this.hpp', 'foreach_fwd.hpp', 'none_t.hpp', 'function_equal.hpp', 'utility', 'container']
    for f in files_to_copy:
        copy(CMAKE_INSTALL_PREFIX[0] + "/include/boost/" + f, SHINING_LIB_DIR + "/" + LIB_NAME + "/boost/" + f)

    lib_basenames = [ "boost_atomic", "boost_chrono", "boost_date_time", "boost_filesystem", "boost_regex", "boost_serialization", "boost_system", "boost_thread", "boost_wave" ]
    split_str = "."

    for i, build_type in enumerate(BUILD_TYPES):
        for f in os.listdir(CMAKE_INSTALL_PREFIX[i] + "/lib"):
            tmp = f[3:].split(split_str)
            if tmp[0] in lib_basenames and f.endswith(BOOST_VERSION_STRING):
                copy(CMAKE_INSTALL_PREFIX[i] + "/lib/" + f, SHINING_LIB_DIR + "/" + LIB_NAME + "/stage/lib/" + f)
    
    with pushd(SHINING_LIB_DIR):
        os.system("tar zcfv " + LIB_NAME + ".tgz " + LIB_NAME)

def build_oiio():
    LIB_NAME="OpenImageIO_" + OIIO_SUFFIX

    GIT_TAG="Release-" + OIIO_VERSION
    CLONE_DIR="repo-" + LIB_NAME
    git("clone https://github.com/OpenImageIO/oiio " + CLONE_DIR)
    with pushd(CLONE_DIR):
        git('config user.email "laurent.noel@ubisoft.com"')
        git('config user.name "Laurent NOEL"')
        git("checkout " + GIT_TAG)
        git("rebase --abort")
        if os.path.exists(SCRIPT_DIR + "/OIIO_shining_" + OIIO_SUFFIX + ".patch"):
            git("am --signoff < " + SCRIPT_DIR + "/OIIO_shining_" + OIIO_SUFFIX + ".patch") # Apply patch for the version

    INSTALL_PREFIX = [ ROOT_DIR + "/dist-" + LIB_NAME + x for x in BUILD_DIR_SUFFIX ]
    BUILD_DIR = [ ROOT_DIR + "/build-" + LIB_NAME + x for x in BUILD_DIR_SUFFIX ]

    for dir in INSTALL_PREFIX:
        mkdir(dir)
    for dir in BUILD_DIR:
        mkdir(dir)

    for i, build_type in enumerate(BUILD_TYPES):
        with pushd(BUILD_DIR[i]):
            cmake(" -DCMAKE_BUILD_TYPE=" + build_type +
                " -DCMAKE_INSTALL_PREFIX=" + INSTALL_PREFIX[i] +
                " -DCMAKE_PREFIX_PATH=" + CMAKE_INSTALL_PREFIX[i] +
                " -DVERBOSE=1" +
                " -DUSE_SIMD=sse4.2" +
                " -USE_CPP11=1" +
                " -DILMBASE_HOME=" + CMAKE_INSTALL_PREFIX[i] +
                " -DOPENEXR_HOME=" + CMAKE_INSTALL_PREFIX[i] +
                " -DBOOST_ROOT=" + CMAKE_INSTALL_PREFIX[i] +
                " -DUSE_PTEX=0"
                " -DUSE_FIELD3D=0"
                " -DUSE_PYTHON=0"
                " -DUSE_QT=0"
                " -DUSE_NUKE=0"
                " -DUSE_FFMPEG=0"
                " -DUSE_GIF=0"
                " -DUSE_FREETYPE=0"
                " -DUSE_OCIO=0"
                " -DUSE_OPENCV=0"
                " -DUSE_OPENJPEG=0"
                " -DUSE_OPENSSL=0"
                " -DUSE_OPENGL=0"
                " -DUSE_JPEGTURBO=0"
                " -DUSE_LIBRAW=0"
                " -DBUILD_TESTING=0"
                " -DOIIO_BUILD_TOOLS=0"
                " -DOIIO_BUILD_TESTS=0"
                " -DINSTALL_DOCS=0"
                " -DINSTALL_FONTS=0"
                " -DBoost_REGEX_LIBRARY_RELEASE=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libboost_regex.so.1.57.0"
                " -DBoost_FILESYSTEM_LIBRARY_RELEASE=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libboost_filesystem.so.1.57.0"
                " -DBoost_SYSTEM_LIBRARY_RELEASE=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libboost_system.so.1.57.0"
                " -DBoost_THREAD_LIBRARY_RELEASE=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libboost_thread.so.1.57.0"
                " ../" + CLONE_DIR)
            build("install", build_type)
            copyfiles(INSTALL_PREFIX[i], CMAKE_INSTALL_PREFIX[i])

    OUTPUT = SHINING_LIB_DIR + "/" + LIB_NAME
    if os.path.exists(OUTPUT + ".tgz"):
        return
    
    print("Build OpenImageIO archive " + OUTPUT + ".tgz for shining...")

    mkdir(OUTPUT + "/include")
    copy(CMAKE_INSTALL_PREFIX[0] + "/include/OpenEXR", OUTPUT + "/include/OpenEXR")
    copy(CMAKE_INSTALL_PREFIX[0] + "/include/OpenImageIO", OUTPUT + "/include/OpenImageIO")

    for i, build_type in enumerate(BUILD_TYPES):
        mkdir(OUTPUT + "/lib")
        input_lib_files = [ "libHalf.so.12.0.0", "libIex-2_2.so.12.0.0", "libIlmImf-2_2.so.22.0.0", "libIlmThread-2_2.so.12.0.0", "libImath-2_2.so.12.0.0"]
        output_lib_files = ["libHalf.so.12", "libIex-2_2.so.12", "libIlmImf-2_2.so.22", "libIlmThread-2_2.so.12", "libImath-2_2.so.12"]
        for k, f in enumerate(input_lib_files):
            copy(CMAKE_INSTALL_PREFIX[i] + "/lib/" + f, OUTPUT + "/lib/" + output_lib_files[k])

        lib64_files = [ "libOpenImageIO.so."+OIIO_SHORT_VERSION ]
        for f in lib64_files:
            copy(CMAKE_INSTALL_PREFIX[i] + "/lib64/" + f, OUTPUT + "/lib/" + f)
    
    with pushd(SHINING_LIB_DIR):
        os.system("tar zcfv " + LIB_NAME + ".tgz " + LIB_NAME)

def build_OSL():
    LIB_NAME="OSL_" + OSL_SUFFIX

    GIT_TAG = "Release-" + OSL_VERSION
    CLONE_DIR = "repo-" + LIB_NAME
    git("clone https://github.com/imageworks/OpenShadingLanguage " + CLONE_DIR)
    with pushd(CLONE_DIR):
        git('config user.email "laurent.noel@ubisoft.com"')
        git('config user.name "Laurent NOEL"')
        git("checkout " + GIT_TAG)
        git("rebase --abort") # Abort current rebase if necessary (Release-1.7.3)
        if os.path.exists(SCRIPT_DIR + "/OSL_shining_" + OSL_SUFFIX + ".patch"):
            git("am --signoff < " + SCRIPT_DIR + "/OSL_shining_" + OSL_SUFFIX + ".patch") # Apply patch for the version
    
    INSTALL_PREFIX = [ ROOT_DIR + "/dist-" + LIB_NAME + x for x in BUILD_DIR_SUFFIX ]
    BUILD_DIR = [ ROOT_DIR + "/build-" + LIB_NAME + x for x in BUILD_DIR_SUFFIX ]

    for dir in INSTALL_PREFIX:
        mkdir(dir)
    for dir in BUILD_DIR:
        mkdir(dir)

    for i, build_type in enumerate(BUILD_TYPES):
        with pushd(BUILD_DIR[i]):
            cmake(" -DCMAKE_BUILD_TYPE=" + build_type +
                " -DCMAKE_INSTALL_PREFIX=" + INSTALL_PREFIX[i] +
                " -DCMAKE_PREFIX_PATH=" + CMAKE_INSTALL_PREFIX[i] +
                " -DVERBOSE=1" +
                " -DUSE_SIMD=sse4.2" +
                " -DUSE_PARTIO=0" +
                " -DOPENEXR_IEX_LIBRARY=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libIex-2_2.so"
                " -DOPENEXR_ILMIMF_LIBRARY=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libIlmImf-2_2.so"
                " -DOPENEXR_ILMTHREAD_LIBRARY=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libIlmThread-2_2.so"
                " -DOPENEXR_IMATH_LIBRARY=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libImath-2_2.so"
                " -DILMBASE_HOME=" + CMAKE_INSTALL_PREFIX[i] +
                " -DOPENEXR_HOME=" + CMAKE_INSTALL_PREFIX[i] +
                " -DBOOST_ROOT=" + CMAKE_INSTALL_PREFIX[i] +
                " -DOSL_BUILD_TESTS=0" +
                " -DLLVM_DIRECTORY=" + CMAKE_INSTALL_PREFIX[i] +
                " -DLLVM_STATIC=1" +
                " -DUSE_LLVM_BITCODE=0" +
                " -DSTOP_ON_WARNING=0" +
                " -DBoost_REGEX_LIBRARY_RELEASE=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libboost_regex.so.1.57.0"
                " -DBoost_FILESYSTEM_LIBRARY_RELEASE=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libboost_filesystem.so.1.57.0"
                " -DBoost_SYSTEM_LIBRARY_RELEASE=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libboost_system.so.1.57.0"
                " -DBoost_THREAD_LIBRARY_RELEASE=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libboost_thread.so.1.57.0"
                " -DBoost_WAVE_LIBRARY_RELEASE=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libboost_wave.so.1.57.0"
                " -DBoost_CHRONO_LIBRARY_RELEASE=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libboost_chrono.so.1.57.0"
                " ../" + CLONE_DIR)
            os.environ["LD_LIBRARY_PATH"] = CMAKE_INSTALL_PREFIX[0] + "/lib" + (":" + os.environ["LD_LIBRARY_PATH"] if "LD_LIBRARY_PATH" in os.environ is not "" else "")
            print("LD_LIBRARY_PATH = {}".format(os.environ["LD_LIBRARY_PATH"]))
            build("install", build_type)
            copyfiles(INSTALL_PREFIX[i], CMAKE_INSTALL_PREFIX[i])

    OUTPUT = SHINING_LIB_DIR + "/" + LIB_NAME
    if os.path.exists(OUTPUT + ".tgz"):
        return
    
    print("Build OSL archive " + OUTPUT + ".tgz for shining...")

    mkdir(OUTPUT + "/include")
    copy(CMAKE_INSTALL_PREFIX[0] + "/include/OSL", OUTPUT + "/include/OSL")

    mkdir(OUTPUT + "/shaders")
    copy(CMAKE_INSTALL_PREFIX[0] + "/shaders/stdosl.h", OUTPUT + "/shaders/stdosl.h")

    for i, build_type in enumerate(BUILD_TYPES):
        mkdir(OUTPUT + "/lib/")
        mkdir(OUTPUT + "/bin/")
        lib_files = ["liboslcomp.so."+OSL_SHORT_VERSION, "liboslexec.so."+OSL_SHORT_VERSION]
        for f in lib_files:
            copy(CMAKE_INSTALL_PREFIX[i] + "/lib64/" + f, OUTPUT + "/lib/" + f)
        copy(CMAKE_INSTALL_PREFIX[i] + "/bin/oslc", OUTPUT + "/bin/oslc.bin")
        copy(SCRIPT_DIR + "/oslc", OUTPUT + "/bin/oslc")

    with pushd(SHINING_LIB_DIR):
    	os.system("tar zcfv " + LIB_NAME + ".tgz " + LIB_NAME)

def build_embree():
    LIB_NAME="embree_" + EMBREE_SUFFIX
    BUILD_DIR = [ ROOT_DIR + "/build-" + LIB_NAME + x for x in BUILD_DIR_SUFFIX ]
    GIT_TAG = "v" + EMBREE_VERSION
    CLONE_DIR = "repo-" + LIB_NAME
    INSTALL_PREFIX = [ os.path.join(ROOT_DIR, "dist-" + LIB_NAME + "-" + build_type) for build_type in BUILD_TYPES ]

    if not os.path.exists(CLONE_DIR):
        git("clone https://github.com/embree/embree.git " + CLONE_DIR)
        with pushd(CLONE_DIR):
            git("checkout " + GIT_TAG)
    
    for i, build_type in enumerate(BUILD_TYPES):
        cmake_build_type = build_type
        tbb_lib = "libtbb.so.2"
        tbb_malloc_lib = "libtbbmalloc.so.2"
        if not os.path.exists(BUILD_DIR[i]):
            mkdir(BUILD_DIR[i])
            with pushd(BUILD_DIR[i]):
                cmake(" -DCMAKE_BUILD_TYPE=" + cmake_build_type +
                    " -DCMAKE_INSTALL_PREFIX=" + INSTALL_PREFIX[i] +
                    " -DEMBREE_TASKING_SYSTEM=TBB" +
                    " -DEMBREE_TBB_ROOT=" + SHINING_3RD_PARTY_DIR + "/tbb-2017.1" +
                    " -DTBB_INCLUDE_DIR=" + SHINING_3RD_PARTY_DIR + "/tbb-2017.1/include" +
                    " -DTBB_LIBRARY=" + SHINING_3RD_PARTY_DIR + "/tbb-2017.1/lib/intel64/gcc4.7/" + tbb_lib +
                    " -DTBB_LIBRARY_MALLOC=" + SHINING_3RD_PARTY_DIR + "/tbb-2017.1/lib/intel64/gcc4.7/" + tbb_malloc_lib +
                    " -DEMBREE_IGNORE_INVALID_RAYS=1" + 
                    " -DEMBREE_RAY_MASK=1" + 
                    " -DEMBREE_MAX_ISA=AVX"
                    " -DEMBREE_ISPC_SUPPORT=0" + 
                    " -DEMBREE_STATIC_LIB=0" + 
                    " -DEMBREE_TUTORIALS=0" + 
                    " " + ROOT_DIR + "/" + CLONE_DIR)
        with pushd(BUILD_DIR[i]):
            build("install", cmake_build_type)

    OUTPUT = SHINING_LIB_DIR + "/" + LIB_NAME
    if os.path.exists(OUTPUT + ".tgz"):
        return
    
    print("Build embree archive " + OUTPUT + ".tgz for shining...")

    copy(INSTALL_PREFIX[0] + "/include", OUTPUT + "/include")

    for i, build_type in enumerate(BUILD_TYPES):
        mkdir(OUTPUT + "/lib/")
        copy(INSTALL_PREFIX[i] + "/lib64/libembree.so." + EMBREE_VERSION, OUTPUT + "/lib/libembree.so.2")
        lib_files = ["libtbb.so.2", "libtbbmalloc.so.2"]
        for f in lib_files:
            copy(SHINING_3RD_PARTY_DIR + "/tbb-2017.1/lib/intel64/gcc4.7/" + f, OUTPUT + "/lib/" + f)

    with pushd(SHINING_LIB_DIR):
        os.system("tar zcfv " + LIB_NAME + ".tgz " + LIB_NAME)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = 'Shining third party builder')
    parser.add_argument("--thirdPartyCompilation", \
        help = "Path to a directory containing some precompiled libraries. Default to Vertigo 3rdPartyCompilation directory.")
    parser.add_argument("--libs", help = "List of libs to build. If not specified, build all libs.", type = valid_list)

    args = parser.parse_args()

    if not args.thirdPartyCompilation:
        args.thirdPartyCompilation = SHINING_3RD_PARTY_DIR

    if args.libs:
        libs = set(args.libs)
    else:
        libs = set(["openexr", "llvm", "boost", "oiio", "osl", "embree"])

    THIRD_PARTY_DIR = args.thirdPartyCompilation
    BUILD_DIR_SUFFIX = [ "-gmake-" + build_type for build_type in BUILD_TYPES ]
    CMAKE_GENERATOR = "Unix Makefiles"

    for install_prefix in CMAKE_INSTALL_PREFIX:
        mkdir(os.path.join(install_prefix, "include"))
        mkdir(os.path.join(install_prefix, "lib"))
        mkdir(os.path.join(install_prefix, "bin"))

    mkdir(SHINING_LIB_DIR)

    # get_precompiled_libraries()
    if "boost" in libs:
        build_boost()
    if "openexr" in libs:
        build_openexr()
    if "llvm" in libs:
        build_llvm()
    if "oiio" in libs:
        build_oiio()
    if "osl" in libs:
        build_OSL()
    if "embree" in libs:
        build_embree()
